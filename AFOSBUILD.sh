source /opt/ANDRAX/PYENV/python3/bin/activate

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Load PYENV... PASS!"
else
  # houston we have a problem
  exit 1
fi

/opt/ANDRAX/PYENV/python3/bin/pip3 install wheel
/opt/ANDRAX/PYENV/python3/bin/pip3 install capstone filebytes keystone-engine pyvex

/opt/ANDRAX/PYENV/python3/bin/pip3 install .

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
